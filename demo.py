import numpy as np
import matplotlib.pyplot as plt
from math import sqrt

e=0.5 #the eccentricity
a=0.25 #the semi-major axis
T=2*np.pi #the orbital period

r0=a**3*(2*np.pi/T)**2 #r0=GM
rp=a*(1+e) #the aphelion
rm=a*(1-e) #the perihelion
E=-r0/(2*a) #Energy
L=sqrt(a*r0*(1-e**2)) #Angular momentum
Ls=L/sqrt(1-r0*(3+e**2)/(1-e**2)/a) #Energy in Schwarzchild metric
Es=1+2*E*(1-r0/(a-r0*(3+e**2)/(1-e**2))) #Angular momentum in Schwarzchild metric

def f(pol):
    r=pol[0]
    global p
    if r>rp:
        r=2*rp-r    
        p=-p
    elif r<rm:
        r=2*rm-r
        p=-p
    dr=sqrt(2*E+2*r0/r-(L/r)**2)
    return np.array([p*dr,L/r**2])

def rk4(pol,h):
    k1=h*f(pol)
    k2=h*f(pol+k1/2)
    k3=h*f(pol+k2/2)
    k4=h*f(pol+k3)
    return pol+(k1+2*k2+2*k3+k4)/6

def fs(pol):
    r=pol[0]
    global p
    if r>rp:
        r=2*rp-r    
        p=-1
    elif r<rm:
        r=2*rm-r
        p=1
    dr=sqrt(Es-(1-2*r0/r)*((Ls/r)**2+1))
    return np.array([p*dr,Ls/r**2])

def rk4s(pol,h):
    k1=h*fs(pol)
    k2=h*fs(pol+k1/2)
    k3=h*fs(pol+k2/2)
    k4=h*fs(pol+k3)
    return pol+(k1+2*k2+2*k3+k4)/6

#initial conditions
t=0
p=1
pol=np.array([1.00001*rm,0])
Pol=np.array([pol])
h=T/1000 #time step
while t<6*T: #the main loop
    pol=rk4(pol,h)
    Pol=np.append(Pol,np.array([pol]),axis=0)
    t+=h
plt.polar(Pol[:,1],Pol[:,0])

t=0
p=1
pols=np.array([1.00001*rm,0])
Pols=np.array([pols])
while t<6*T:
    pols=rk4s(pols,h)
    Pols=np.append(Pols,np.array([pols]),axis=0)
    t+=h
plt.polar(Pols[:,1],Pols[:,0])

plt.show()